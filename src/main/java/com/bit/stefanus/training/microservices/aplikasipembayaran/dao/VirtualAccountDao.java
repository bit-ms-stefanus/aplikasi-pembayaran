package com.bit.stefanus.training.microservices.aplikasipembayaran.dao;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.aplikasipembayaran.entity.VirtualAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VirtualAccountDao extends PagingAndSortingRepository<VirtualAccount, String> {
}
