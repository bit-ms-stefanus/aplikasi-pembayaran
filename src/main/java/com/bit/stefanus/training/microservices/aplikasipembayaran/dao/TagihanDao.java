package com.bit.stefanus.training.microservices.aplikasipembayaran.dao;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.aplikasipembayaran.entity.Tagihan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TagihanDao extends PagingAndSortingRepository<Tagihan, String> {
    Tagihan findByNomor(String nomorTagihan);
}
