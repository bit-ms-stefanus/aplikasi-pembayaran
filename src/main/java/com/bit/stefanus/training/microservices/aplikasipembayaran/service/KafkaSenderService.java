package com.bit.stefanus.training.microservices.aplikasipembayaran.service;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.aplikasipembayaran.dao.CustomerDao;
import com.bit.stefanus.training.microservices.aplikasipembayaran.dto.TagihanRequest;
import com.bit.stefanus.training.microservices.aplikasipembayaran.entity.Customer;
import com.bit.stefanus.training.microservices.aplikasipembayaran.entity.Tagihan;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaSenderService {

    @Value("${kafka.topic.tagihan.request:tagihan-request-dev}")
    private String topicTagihanRequest;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CustomerDao customerDao;

    public void kirim(Tagihan tagihan) throws JsonProcessingException {
        Customer c = customerDao.findById(tagihan.getCustomer().getId()).get();

        TagihanRequest tr = new TagihanRequest();
        tr.setEmail(c.getEmail());
        tr.setJatuhTempo(tagihan.getJatuhTempo());
        tr.setKeteranganTagihan(tagihan.getKeterangan());
        tr.setNilaiTagihan(tagihan.getNilai());
        tr.setNama(c.getNama());
        tr.setNoHp(c.getNoHp());
        tr.setNomorTagihan(tagihan.getNomor());

        String tagihanRequestJson
                = objectMapper.writeValueAsString(tr);

        log.info("Tagihan Request : {}", tagihanRequestJson);
        kafkaTemplate.send(topicTagihanRequest, tagihanRequestJson);
    }
}