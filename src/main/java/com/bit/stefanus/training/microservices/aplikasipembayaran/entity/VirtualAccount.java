package com.bit.stefanus.training.microservices.aplikasipembayaran.entity;

/**
 *
 * @author Stefanus
 */
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class VirtualAccount {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_tagihan")
    private Tagihan tagihan;

    @NotNull
    @NotEmpty
    private String bank;

    @NotNull
    @NotEmpty
    private String nomorVa;

}
