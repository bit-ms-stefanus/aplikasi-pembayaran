package com.bit.stefanus.training.microservices.aplikasipembayaran.dto;

/**
 *
 * @author Stefanus
 */
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TagihanResponse {

    private String nomorTagihan;
    private String nama;
    private BigDecimal nilaiTagihan;
    private String bank;
    private String nomorVa;
}