package com.bit.stefanus.training.microservices.aplikasipembayaran.dao;

import com.bit.stefanus.training.microservices.aplikasipembayaran.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
