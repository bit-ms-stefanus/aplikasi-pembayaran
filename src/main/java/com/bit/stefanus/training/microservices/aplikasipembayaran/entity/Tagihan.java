/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.stefanus.training.microservices.aplikasipembayaran.entity;

/**
 *
 * @author Stefanus
 */
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class Tagihan {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 100)
    private String nomor;

    @NotNull
    @NotEmpty
    private String keterangan;

    @NotNull
    @Min(1000)
    private BigDecimal nilai;

    @NotNull
    private LocalDate jatuhTempo = LocalDate.now().plusMonths(1);
}