package com.bit.stefanus.training.microservices.aplikasipembayaran.controller;

/**
 *
 * @author Stefanus
 */

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class BackendInfoController {

    @GetMapping("/api/backendinfo")
    public Map<String, Object> backendInfo(HttpServletRequest request) {
        String alamatIp = request.getLocalAddr();
        Integer port = request.getLocalPort();

        Map<String, Object> hasil = new LinkedHashMap<>();
        hasil.put("ipAddress", alamatIp);
        hasil.put("port",port);
        hasil.put("hostname", request.getLocalName());

        return hasil;
    }
}

