/**
 * Author:  Stefanus
 * Created: 24 Mar 2020
 */
create table tagihan
(
    id          varchar(36),
    id_customer varchar(36)    not null,
    nomor       varchar(100)   not null,
    keterangan  varchar(255)   not null,
    nilai       decimal(19, 2) not null,
    jatuh_tempo date           not null,
    primary key (id),
    unique (nomor),
    foreign key (id_customer) references customer (id)
);