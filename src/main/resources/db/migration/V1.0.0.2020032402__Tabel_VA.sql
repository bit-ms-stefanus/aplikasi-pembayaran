/**
 * Author:  Stefanus
 * Created: 24 Mar 2020
 */
create table virtual_account
(
    id         varchar(36),
    id_tagihan varchar(36)  not null,
    bank       varchar(100) not null,
    nomor_va   varchar(100) not null,
    primary key (id)
);


